import React, { Component } from "react";
import { Item, Button } from "./styles";
import { observer, inject } from "mobx-react";

class PinnedItem extends Component {
	onItemClick = () => {
		this.props.store.pinnedItem.toggleCompleted();
	};

	render() {
		const { title, completed } = this.props.store.pinnedItem;
		return (
			<Item completed={completed}>
				<div onClick={this.onItemClick}>
					<h3>Pinned Item</h3>
					<p>Название: {title}</p>
				</div>
				<Button onClick={this.props.unpin}>Открепить</Button>
			</Item>
		);
	}
}

export default inject("store")(observer(PinnedItem));
