import React, { Component } from "react";
import { observer, inject } from "mobx-react";

import { List } from "./styles";
import ItemComponent from "./Item";

class Todolist extends Component {

	onDeleteButtonClick = (index, id) => {
		this.props.store.deleteItem(index, id);
	};

	onPinnedButttonClick = (id) => {
		this.props.store.pinItem(id);
	};

	render() {
		const { list } = this.props.store;
		return (
			<List>
				{list.map((item, index) => (
					<ItemComponent
						item={item}
						key={item.id}
						index={index}
						pinned={this.onPinnedButttonClick}
						delete={this.onDeleteButtonClick}
					/>
				))}
			</List>
		);
	}
}

export default inject("store")(observer(Todolist));
