import styled from "styled-components";

export const List = styled.ul`
	list-style: none;
	padding: 0px;
`;

export const Item = styled.li`
	padding: 10px;
	border-radius: 10px;
	color: #fff;
	transition: 0.2s background ease;
	margin-bottom: 10px;
	display: flex;
	justify-content: space-between;
	cursor: pointer;
	background: ${({ completed }) => (completed ? "#08da08" : "#cc0000")};

	span {
		width: 100%;
	}
`;

export const Button = styled.button`
	border: none;
	background: #fff;
	padding: 5px 10px;
	border-radius: 5px;
	margin-left: 5px;
`;
