import React, { Component } from "react";
import { Item, Button } from "../styles";
import { observer } from "mobx-react";

class ItemComponent extends Component {
	onItemClick = index => {
		this.props.item.toggleCompleted();
	};

	render() {
		const { completed, title, id } = this.props.item;
		const { index } = this.props;
		return (
			<Item completed={completed} key={this.props.index}>
				<span onClick={() => this.onItemClick(this.props.index)}>{title}</span>
				<Button onClick={() => this.props.pinned(id)}>закрепить</Button>
				<Button onClick={() => this.props.delete(index, id)}>удалить</Button>
			</Item>
		);
	}
}

export default observer(ItemComponent);
