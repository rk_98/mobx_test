import React, { useContext } from "react";
import { Observer, useObservable } from "mobx-react-lite";
import { Item, Button } from "../PinnedItem/styles";
import { StoreContext } from "../..";

function PinnedItemFunc(props) {
	const store = useContext(StoreContext);
	const item = useObservable(store.pinnedItem);
	return (
		<Observer>
			{() => (
				<Item completed={item.completed}>
					<div onClick={item.toggleCompleted}>
						<h3>Pinned Item Func</h3>
						<p>Названия: {item.title}</p>
					</div>
					<Button onClick={props.unpin}>Открепить</Button>
				</Item>
			)}
		</Observer>
	);
}

export default PinnedItemFunc;