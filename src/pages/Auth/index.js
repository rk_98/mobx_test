import React, { Component } from "react";
import {AuthButton, FlexCenterContainer} from './styles'

class AuthPage extends Component {
	render() {
		return (
			<FlexCenterContainer>
				<AuthButton to="/main">Login</AuthButton>
			</FlexCenterContainer>
		);
	}
}

export default AuthPage;
