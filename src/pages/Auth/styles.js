import styled from 'styled-components'
import { Link } from "react-router-dom"

export const AuthButton = styled(Link)`
  border: 1px solid;
  padding: 10px 20px;
  border-radius: 5px;
  text-decoration: none
`

export const FlexCenterContainer = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 20px)
`