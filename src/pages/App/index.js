import React, { Component } from "react";
import { observer, inject } from "mobx-react";

import TodoList from "../../containers/TodoList";
import PinnedItem from "../../containers/PinnedItem";
import { Container } from "./styles";
import nanoid from "nanoid";
import PinnedItemFunc from "../../containers/PinnedItemFunc";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = { inputValue: "" };
	}

	componentDidMount() {
		this.props.store.getCities();
	}

	render() {
		const { pinnedItem } = this.props.store;
		return (
			<>
				{this.props.store.loading === "pending" ? (
					"loading"
				) : (
					<Container>
						<>
							{this.props.store.unComplitedTodos
								? "all todos complited"
								: "not all todos complited"}
						</>
						{pinnedItem !== null && (
							<>
								<PinnedItemFunc unpin={this.unPinItem} />
								<PinnedItem unpin={this.unPinItem} />
							</>
						)}
						<h3>Todo list</h3>
						<form>
							<input
								type="text"
								onChange={this.handleInputChange}
								placeholder="Введите строку"
								required
							/>
							<button type="button" onClick={this.createNewItem}>
								Ок
							</button>
						</form>
						{this.props.store.list.length !== 0 && <TodoList />}
					</Container>
				)}
			</>
		);
	}

	createNewItem = () => {
		this.props.store.createNewItem(this.state.inputValue, nanoid(4));
	};

	unPinItem = () => {
		this.props.store.unPinItem();
	};

	handleInputChange = event => {
		this.setState({ inputValue: event.target.value });
	};
}

export default inject("store")(observer(App));
