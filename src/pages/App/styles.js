import styled from 'styled-components'

export const Container = styled.div`
  padding: 20px;
  max-width: calc(100% - 40px);
`