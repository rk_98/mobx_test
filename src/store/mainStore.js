import { types, flow } from "mobx-state-tree";
import nanoid from "nanoid";

const Item = types
	.model({
		title: types.maybe(types.string),
		completed: false,
		id: types.identifier
	})
	.actions(self => {
		return {
			toggleCompleted() {
				self.completed = !self.completed;
			}
		};
	});

const Store = types
	.model({
		list: types.array(Item),
		pinnedItem: types.maybeNull(types.reference(Item)),
		loading: types.maybe(types.string)
	})
	.actions(self => {
		const getCities = flow(function*() {
			try {
				self.loading = "pending";
				let data = yield fetch("https://indian-cities-api-nocbegfhqg.now.sh/cities");
				data = yield data.json();
				data.splice(9, 5151);

				for (let i = 0; i < data.length; i++) {
					data[i].id = nanoid(4);
					data[i].title = data[i].City;

					// delete unnecessary props
					delete data[i].City;
					delete data[i].District;
					delete data[i].State;
				}

				self.list = data;
				self.loading = "done";
			} catch (err) {
				self.loading = "error";
				console.log(err.message);
			}
			return self.list;
		});

		return {
			getCities,
			createNewItem(title, id) {
				self.list.push({ title: title, id: id });
			},
			deleteItem(index, id) {
				self.list[index].id === id && (self.pinnedItem = null);
				self.list.splice(index, 1);
			},
			pinItem(id) {
				self.pinnedItem = id;
			},
			unPinItem() {
				self.pinnedItem = null;
			}
		};
	})
	.views(self => ({
		get unComplitedTodos() {
			const unComplitedItems = self.list.filter(item => item.completed !== true);
			return !unComplitedItems.length;
		}
	}));

export default Store;
