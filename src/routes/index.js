import React, {Component} from 'react'
import {Route} from 'react-router'
import App from '../pages/App'
import AuthPage from '../pages/Auth'

class RootRouter extends Component{
  render(){
    return(
      <>
        <Route path="/" component={AuthPage} exact/>
        <Route path="/main" component={App}/>
      </>
    )
  }
}

export default RootRouter;