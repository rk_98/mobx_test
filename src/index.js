import React from "react";
import ReactDOM from "react-dom";
import Store from "./store/mainStore";
import { Provider } from "mobx-react";
import { BrowserRouter, Switch } from "react-router-dom";
import RootRouter from "./routes";

const store = Store.create();
export const StoreContext = React.createContext();

ReactDOM.render(
	<Provider store={store}>
		<StoreContext.Provider value={store}>
			<BrowserRouter>
				<Switch>
					<RootRouter />
				</Switch>
			</BrowserRouter>
		</StoreContext.Provider>
	</Provider>,
	document.getElementById("root")
);
